<?php

namespace jossmp\response\model;

class company extends \jossmp\response\obj
{
	private $id_sunat             = NULL;
	var $ruc                      = NULL;
	var $razon_social             = NULL;
	var $direccion                = NULL;
	var $estado                   = NULL;
	var $condicion                = NULL;
	var $tipo                     = NULL;
	var $nombre_comercial         = NULL;
	var $fecha_inscripcion        = NULL;
	var $sistema_emision          = NULL;
	var $actividad_exterior       = NULL;
	var $sistema_contabilidad     = NULL;
	var $comprobante_electronico  = NULL;
	var $ple                      = NULL;
	var $inicio_actividades       = NULL;
	var $actividad_economica      = NULL;
	var $establecimientos         = NULL;
	var $cantidad_trabajadores    = NULL;
	var $representantes_legales   = NULL;
	var $oficio                   = NULL;
	var $ubigeo                   = NULL;
	var $deuda_coactiva           = NULL;
	var $completo                 = NULL;
	var $dir_tipo_via             = NULL;
	var $dir_cod_zona             = NULL;
	var $dir_tipo_zona            = NULL;
	var $dir_num                  = NULL;
	var $dir_interior             = NULL;
	var $dir_lote                 = NULL;
	var $dir_dpto                 = NULL;
	var $dir_manzana              = NULL;
	var $dir_km                   = NULL;
	var $dir_nomb_via             = NULL;
	var $emision_electronica      = NULL;
	var $prefijo                  = NULL;
	var $fecha_registro           = NULL;
	var $fecha_actualizacion      = NULL;

	private function format_fecha($date, $format_in = NULL)
	{
		if ($format_in == NULL) {
			return $date;
		}
		$date = \DateTime::createFromFormat($format_in, $date);

		if ($date === FALSE) {
			return NULL;
		}
		return $date->format("Y-m-d");
	}

	private function calcula_edad($date, $format_in = 'Y-m-d')
	{
		$date = \DateTime::createFromFormat($format_in, $date);
		if ($date === FALSE) {
			return NULL;
		}
		$now = new \DateTime("now");
		$diff = $now->diff($date);

		return $diff->format("%y");
	}
	public function cod_verificacion($num_doc)
	{
		if ($num_doc != "" || strlen($num_doc) == 8) {
			$suma = 0;
			$hash = array(5, 4, 3, 2, 7, 6, 5, 4, 3, 2);
			$suma = 5;
			for ($i = 2; $i < 10; $i++) {
				$suma += ($num_doc[$i - 2] * $hash[$i]);
			}
			$entero = (int) ($suma / 11);

			$digito = 11 - ($suma - $entero * 11);

			if ($digito == 10) {
				$digito = 0;
			} else if ($digito == 11) {
				$digito = 1;
			}
			return $digito;
		}
		return null;
	}
}
