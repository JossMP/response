<?php

namespace jossmp\response\model;

class person extends \jossmp\response\obj
{
	private $id_reniec                  = NULL;

	var $cod_tipo_doc                   = NULL;
	var $tipo_doc                       = NULL;
	var $num_doc                        = NULL;
	var $verificacion                   = NULL;
	var $gvotacion                      = NULL;

	var $nombres                        = NULL;
	var $apellidos                      = NULL;
	var $apellido_paterno               = NULL;
	var $apellido_materno               = NULL;
	var $apellido_matrimonio            = NULL;
	var $nombre_completo                = NULL;
	var $fecha_nacimiento               = NULL;
	var $edad                           = NULL;
	var $estatura                       = NULL;
	var $estado_civil                   = NULL;
	var $cod_sexo                       = NULL;
	var $sexo                           = NULL;
	var $genero                         = NULL;
	var $grado_instruccion              = NULL;
	var $donante                        = NULL;

	var $cod_restriccion                = NULL;
	var $restriccion                    = NULL;
	var $gra_restriccion                = NULL;
	var $cancelacion                    = NULL;

	var $tipo_ficha                     = NULL;
	var $tipo_ficha_imagen              = NULL;
	var $num_imagen                     = NULL;
	var $num_ficha                      = NULL;

	var $fecha_emision                  = NULL;
	var $fecha_inscripcion              = NULL;
	var $fecha_caducidad                = NULL;

	var $dir_tipo                       = NULL;
	var $dir_nombre                     = NULL;
	var $dir_num                        = NULL;
	var $dir_urb                        = NULL;
	var $dir_bloque                     = NULL;
	var $dir_dpto_piso                  = NULL;
	var $dir_etapa                      = NULL;
	var $dir_manzana                    = NULL;
	var $dir_lote                       = NULL;

	var $ubi_dir_dist_desc              = NULL;
	var $ubi_dir_prov_desc              = NULL;
	var $ubi_dir_depa_desc              = NULL;
	var $ubi_dir_pais_desc              = NULL;
	var $ubi_dir_cont_desc              = NULL;
	var $ubi_dir_dist                   = NULL;
	var $ubi_dir_prov                   = NULL;
	var $ubi_dir_depa                   = NULL;
	var $ubi_dir_pais                   = NULL;
	var $ubi_dir_cont                   = NULL;

	var $ubi_nac_dist_desc              = NULL;
	var $ubi_nac_prov_desc              = NULL;
	var $ubi_nac_depa_desc              = NULL;
	var $ubi_nac_pais_desc              = NULL;
	var $ubi_nac_cont_desc              = NULL;
	var $ubi_nac_dist                   = NULL;
	var $ubi_nac_prov                   = NULL;
	var $ubi_nac_depa                   = NULL;
	var $ubi_nac_pais                   = NULL;
	var $ubi_nac_cont                   = NULL;

	var $fecha_fallecimiento            = NULL;
	var $ubi_fallecimiento_dist_desc    = NULL;
	var $ubi_fallecimiento_prov_desc    = NULL;
	var $ubi_fallecimiento_depa_desc    = NULL;
	var $ubi_fallecimiento_pais_desc    = NULL;
	var $ubi_fallecimiento_cont_desc    = NULL;

	var $email                          = NULL;
	var $telefono                       = NULL;
	var $codigo_postal                  = NULL;

	var $padre_nombre                   = NULL;
	var $padre_apellido_paterno         = NULL;
	var $padre_apellido_materno         = NULL;
	var $padre_apellido_matrimonio      = NULL;
	var $padre_nombre_completo          = NULL;
	var $padre_tipo_doc                 = NULL;
	var $padre_num_doc                  = NULL;

	var $madre_nombre                   = NULL;
	var $madre_apellido_paterno         = NULL;
	var $madre_apellido_materno         = NULL;
	var $madre_apellido_matrimonio      = NULL;
	var $madre_nombre_completo          = NULL;
	var $madre_tipo_doc                 = NULL;
	var $madre_num_doc                  = NULL;

	var $declarante_nombre              = NULL;
	var $declarante_apellido_paterno    = NULL;
	var $declarante_apellido_materno    = NULL;
	var $declarante_apellido_matrimonio = NULL;
	var $declarante_nombre_completo     = NULL;
	var $declarante_tipo_doc            = NULL;
	var $declarante_num_doc             = NULL;
	var $declarante_vinculo             = NULL;

	var $fecha_registro                 = NULL;
	var $fecha_actualizacion            = NULL;

	private function format_fecha($date, $format_in = NULL)
	{
		if ($format_in == NULL) {
			return $date;
		}
		$date = \DateTime::createFromFormat($format_in, $date);

		if ($date === FALSE) {
			return NULL;
		}
		return $date->format("Y-m-d");
	}

	private function calcula_edad($date, $format_in = 'Y-m-d')
	{
		$date = \DateTime::createFromFormat($format_in, $date);
		if ($date === FALSE) {
			return NULL;
		}
		$now = new \DateTime("now");
		$diff = $now->diff($date);

		return $diff->format("%y");
	}
	public function cod_verificacion($num_doc)
	{
		if ($num_doc != "" || strlen($num_doc) == 8) {
			$suma = 0;
			$hash = array(5, 4, 3, 2, 7, 6, 5, 4, 3, 2);
			$suma = 5;
			for ($i = 2; $i < 10; $i++) {
				$suma += ($num_doc[$i - 2] * $hash[$i]);
			}
			$entero = (int) ($suma / 11);

			$digito = 11 - ($suma - $entero * 11);

			if ($digito == 10) {
				$digito = 0;
			} else if ($digito == 11) {
				$digito = 1;
			}
			return (string)$digito;
		}
		return null;
	}

	/* FUNCTION SET_ */
	public function set_id_reniec($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->id_reniec = $value;
	}

	public function set_cod_tipo_doc($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->cod_tipo_doc = $value;
	}
	public function set_tipo_doc($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->tipo_doc = $value;
	}
	public function set_num_doc($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->num_doc = $value;
		$this->set_verificacion($this->cod_verificacion($this->num_doc));
	}
	public function set_verificacion($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->verificacion = $value;
	}
	public function set_gvotacion($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->gvotacion = $value;
	}

	public function set_nombres($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->nombres = $value;
		if ($this->apellido_materno != NULL && $this->apellido_paterno != NULL) {
			$this->set_apellidos($this->apellido_paterno . ' ' . $this->apellido_materno);
			if ($this->nombres != NULL) {
				$this->set_nombre_completo($this->nombres . ' ' . $this->apellidos);
			}
		}
	}
	public function set_apellidos($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->apellidos = $value;
	}
	public function set_apellido_paterno($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->apellido_paterno = $value;
		if ($this->apellido_materno != NULL && $this->apellido_paterno != NULL) {
			$this->set_apellidos($this->apellido_paterno . ' ' . $this->apellido_materno);
			if ($this->nombres != NULL) {
				$this->set_nombre_completo($this->nombres . ' ' . $this->apellidos);
			}
		}
	}
	public function set_apellido_materno($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->apellido_materno = $value;
		if ($this->apellido_materno != NULL && $this->apellido_paterno != NULL) {
			$this->set_apellidos($this->apellido_paterno . ' ' . $this->apellido_materno);
			if ($this->nombres != NULL) {
				$this->set_nombre_completo($this->nombres . ' ' . $this->apellidos);
			}
		}
	}
	public function set_apellido_matrimonio($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->apellido_matrimonio = $value;
	}
	public function set_nombre_completo($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->nombre_completo = $value;
	}
	public function set_fecha_nacimiento($value = NULL, $format_in = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->fecha_nacimiento = $this->format_fecha($value, $format_in);

		if ($this->edad === NULL && $this->fecha_nacimiento != NULL) {
			$this->set_edad($this->calcula_edad($this->fecha_nacimiento));
		}
	}
	public function set_edad($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->edad = $value;
	}
	public function set_estatura($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->estatura = $value;
	}
	public function set_estado_civil($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->estado_civil = $value;
	}
	public function set_cod_sexo($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->cod_sexo = $value;
		if ($this->sexo === NULL) {
			$this->sexo = ($this->cod_sexo == 1) ? 'MASCULINO' : (($this->cod_sexo == 2) ? 'FEMENINO' : NULL);
		}
	}
	public function set_sexo($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->sexo = $value;
		if ($this->cod_sexo === NULL) {
			$this->cod_sexo = ($this->sexo == 'MASCULINO') ? '1' : (($this->sexo == 'FEMENINO') ? '2' : NULL);
		}
	}
	public function set_genero($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->genero = $value;
	}

	public function set_grado_instruccion($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->grado_instruccion = $value;
	}
	public function set_donante($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->donante = $value;
	}

	public function set_cod_restriccion($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->cod_restriccion = $value;
	}
	public function set_restriccion($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->restriccion = $value;
	}
	public function set_gra_restriccion($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->gra_restriccion = $value;
	}
	public function set_cancelacion($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->cancelacion = $value;
	}

	public function set_tipo_ficha($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->tipo_ficha = $value;
	}
	public function set_tipo_ficha_imagen($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->tipo_ficha_imagen = $value;
	}
	public function set_num_imagen($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->num_imagen = $value;
	}
	public function set_num_ficha($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->num_ficha = $value;
	}

	public function set_fecha_emision($value = NULL, $format_in = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->fecha_emision = $this->format_fecha($value, $format_in);
	}
	public function set_fecha_inscripcion($value = NULL, $format_in = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->fecha_inscripcion = $this->format_fecha($value, $format_in);
	}
	public function set_fecha_caducidad($value = NULL, $format_in = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->fecha_caducidad = $this->format_fecha($value, $format_in);
	}

	public function set_dir_tipo($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->dir_tipo = $value;
	}
	public function set_dir_nombre($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->dir_nombre = $value;
	}
	public function set_dir_num($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->dir_num = $value;
	}
	public function set_dir_urb($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->dir_urb = $value;
	}
	public function set_dir_bloque($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->dir_bloque = $value;
	}
	public function set_dir_dpto_piso($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->dir_dpto_piso = $value;
	}
	public function set_dir_etapa($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->dir_etapa = $value;
	}
	public function set_dir_manzana($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->dir_manzana = $value;
	}
	public function set_dir_lote($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->dir_lote = $value;
	}

	public function set_ubi_dir_dist_desc($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->ubi_dir_dist_desc = $value;
	}
	public function set_ubi_dir_prov_desc($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->ubi_dir_prov_desc = $value;
	}
	public function set_ubi_dir_depa_desc($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->ubi_dir_depa_desc = $value;
	}
	public function set_ubi_dir_pais_desc($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->ubi_dir_pais_desc = $value;
	}
	public function set_ubi_dir_cont_desc($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->ubi_dir_cont_desc = $value;
	}
	public function set_ubi_dir_dist($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->ubi_dir_dist = $value;
	}
	public function set_ubi_dir_prov($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->ubi_dir_prov = $value;
	}
	public function set_ubi_dir_depa($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->ubi_dir_depa = $value;
	}
	public function set_ubi_dir_pais($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->ubi_dir_pais = $value;
	}
	public function set_ubi_dir_cont($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->ubi_dir_cont = $value;
	}
	public function set_ubi_nac_dist_desc($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->ubi_nac_dist_desc = $value;
	}
	public function set_ubi_nac_prov_desc($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->ubi_nac_prov_desc = $value;
	}
	public function set_ubi_nac_depa_desc($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->ubi_nac_depa_desc = $value;
	}
	public function set_ubi_nac_pais_desc($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->ubi_nac_pais_desc = $value;
	}
	public function set_ubi_nac_cont_desc($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->ubi_nac_cont_desc = $value;
	}
	public function set_ubi_nac_dist($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->ubi_nac_dist = $value;
	}
	public function set_ubi_nac_prov($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->ubi_nac_prov = $value;
	}
	public function set_ubi_nac_depa($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->ubi_nac_depa = $value;
	}
	public function set_ubi_nac_pais($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->ubi_nac_pais = $value;
	}
	public function set_ubi_nac_cont($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->ubi_nac_cont = $value;
	}

	public function set_fecha_fallecimiento($value = NULL, $format_in = 'd/m/Y', $format_out = 'Y-m-d')
	{
		if ($value == null || trim($value) == '') return;

		$this->fecha_fallecimiento = $this->format_fecha($value, $format_in, $format_out);
	}
	public function set_ubi_fallecimiento_dist_desc($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->ubi_fallecimiento_dist_desc = $value;
	}
	public function set_ubi_fallecimiento_prov_desc($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->ubi_fallecimiento_prov_desc = $value;
	}
	public function set_ubi_fallecimiento_depa_desc($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->ubi_fallecimiento_depa_desc = $value;
	}
	public function set_ubi_fallecimiento_pais_desc($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->ubi_fallecimiento_pais_desc = $value;
	}
	public function set_ubi_fallecimiento_cont_desc($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->ubi_fallecimiento_cont_desc = $value;
	}

	public function set_email($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->email = $value;
	}
	public function set_telefono($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->telefono = $value;
	}
	public function set_codigo_postal($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->codigo_postal = $value;
	}

	public function set_padre_nombre($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->padre_nombre = $value;
		$this->padre_nombre_completo = $this->padre_nombre . ' ' . $this->padre_apellido_paterno . ' ' . $this->padre_apellido_materno;
	}
	public function set_padre_apellido_paterno($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->padre_apellido_paterno = $value;
		$this->padre_nombre_completo = $this->padre_nombre . ' ' . $this->padre_apellido_paterno . ' ' . $this->padre_apellido_materno;
	}
	public function set_padre_apellido_materno($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->padre_apellido_materno = $value;
		$this->padre_nombre_completo = $this->padre_nombre . ' ' . $this->padre_apellido_paterno . ' ' . $this->padre_apellido_materno;
	}
	public function set_padre_apellido_matrimonio($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->padre_apellido_matrimonio = $value;
	}
	public function set_padre_nombre_completo($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->padre_nombre_completo = $value;
	}
	public function set_padre_tipo_doc($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->padre_tipo_doc = $value;
	}
	public function set_padre_num_doc($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->padre_num_doc = $value;
	}

	public function set_madre_nombre($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->madre_nombre = $value;
		$this->madre_nombre_completo = $this->madre_nombre . ' ' . $this->madre_apellido_paterno . ' ' . $this->madre_apellido_materno;
	}
	public function set_madre_apellido_paterno($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->madre_apellido_paterno = $value;
		$this->madre_nombre_completo = $this->madre_nombre . ' ' . $this->madre_apellido_paterno . ' ' . $this->madre_apellido_materno;
	}
	public function set_madre_apellido_materno($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->madre_apellido_materno = $value;
		$this->madre_nombre_completo = $this->madre_nombre . ' ' . $this->madre_apellido_paterno . ' ' . $this->madre_apellido_materno;
	}
	public function set_madre_apellido_matrimonio($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->madre_apellido_matrimonio = $value;
	}
	public function set_madre_nombre_completo($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->madre_nombre_completo = $value;
	}
	public function set_madre_tipo_doc($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->madre_tipo_doc = $value;
	}
	public function set_madre_num_doc($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->madre_num_doc = $value;
	}

	public function set_declarante_nombre($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->declarante_nombre = $value;
	}
	public function set_declarante_apellido_paterno($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->declarante_apellido_paterno = $value;
	}
	public function set_declarante_apellido_materno($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->declarante_apellido_materno = $value;
	}
	public function set_declarante_apellido_matrimonio($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->declarante_apellido_matrimonio = $value;
	}
	public function set_declarante_nombre_completo($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->declarante_nombre_completo = $value;
	}
	public function set_declarante_tipo_doc($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->declarante_tipo_doc = $value;
	}
	public function set_declarante_num_doc($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->declarante_num_doc = $value;
	}
	public function set_declarante_vinculo($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->declarante_vinculo = $value;
	}

	public function set_fecha_registro($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->fecha_registro = $value;
	}
	public function set_fecha_actualizacion($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->fecha_actualizacion = $value;
	}

	public function set_fuente_padron($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->fuente_padron = $value;
	}
	public function set_fuente_rrcc($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->fuente_rrcc = $value;
	}
	public function set_fuente_api($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->fuente_api = $value;
	}
	public function set_fuente_contacto($value = NULL)
	{
		if ($value == null || trim($value) == '') return;

		$this->fuente_contacto = $value;
	}


	/* FUNCTION GET_ */
	public function get_id_reniec()
	{
		return $this->id_reniec;
	}
	public function get_cod_tipo_doc()
	{
		return $this->cod_tipo_doc;
	}
	public function get_tipo_doc()
	{
		return $this->tipo_doc;
	}
	public function get_num_doc()
	{
		return $this->num_doc;
	}
	public function get_verificacion()
	{
		return $this->verificacion;
	}
	public function get_gvotacion()
	{
		return $this->gvotacion;
	}

	public function get_nombres()
	{
		return $this->nombres;
	}
	public function get_apellidos()
	{
		return $this->apellidos;
	}
	public function get_apellido_paterno()
	{
		return $this->apellido_paterno;
	}
	public function get_apellido_materno()
	{
		return $this->apellido_materno;
	}
	public function get_apellido_matrimonio()
	{
		return $this->apellido_matrimonio;
	}
	public function get_nombre_completo()
	{
		return $this->nombre_completo;
	}
	public function get_fecha_nacimiento()
	{
		return $this->fecha_nacimiento;
	}
	public function get_edad()
	{
		return $this->edad;
	}
	public function get_estatura()
	{
		return $this->estatura;
	}
	public function get_estado_civil()
	{
		return $this->estado_civil;
	}
	public function get_cod_sexo()
	{
		return $this->cod_sexo;
	}
	public function get_sexo()
	{
		return $this->sexo;
	}
	public function get_genero()
	{
		return $this->genero;
	}

	public function get_grado_instruccion()
	{
		return $this->grado_instruccion;
	}
	public function get_donante()
	{
		return $this->donante;
	}

	public function get_cod_restriccion()
	{
		return $this->cod_restriccion;
	}
	public function get_restriccion()
	{
		return $this->restriccion;
	}
	public function get_gra_restriccion()
	{
		return $this->gra_restriccion;
	}
	public function get_cancelacion()
	{
		return $this->cancelacion;
	}

	public function get_tipo_ficha()
	{
		return $this->tipo_ficha;
	}
	public function get_tipo_ficha_imagen()
	{
		return $this->tipo_ficha_imagen;
	}
	public function get_num_imagen()
	{
		return $this->num_imagen;
	}
	public function get_num_ficha()
	{
		return $this->num_ficha;
	}


	public function get_fecha_emision()
	{
		return $this->fecha_emision;
	}
	public function get_fecha_inscripcion()
	{
		return $this->fecha_inscripcion;
	}
	public function get_fecha_caducidad()
	{
		return $this->fecha_caducidad;
	}

	public function get_dir_tipo()
	{
		return $this->dir_tipo;
	}
	public function get_dir_nombre()
	{
		return $this->dir_nombre;
	}
	public function get_dir_num()
	{
		return $this->dir_num;
	}
	public function get_dir_urb()
	{
		return $this->dir_urb;
	}
	public function get_dir_bloque()
	{
		return $this->dir_bloque;
	}
	public function get_dir_dpto_piso()
	{
		return $this->dir_dpto_piso;
	}
	public function get_dir_etapa()
	{
		return $this->dir_etapa;
	}
	public function get_dir_manzana()
	{
		return $this->dir_manzana;
	}
	public function get_dir_lote()
	{
		return $this->dir_lote;
	}

	public function get_ubi_dir_dist_desc()
	{
		return $this->ubi_dir_dist_desc;
	}
	public function get_ubi_dir_prov_desc()
	{
		return $this->ubi_dir_prov_desc;
	}
	public function get_ubi_dir_depa_desc()
	{
		return $this->ubi_dir_depa_desc;
	}
	public function get_ubi_dir_pais_desc()
	{
		return $this->ubi_dir_pais_desc;
	}
	public function get_ubi_dir_cont_desc()
	{
		return $this->ubi_dir_cont_desc;
	}
	public function get_ubi_dir_dist()
	{
		return $this->ubi_dir_dist;
	}
	public function get_ubi_dir_prov()
	{
		return $this->ubi_dir_prov;
	}
	public function get_ubi_dir_depa()
	{
		return $this->ubi_dir_depa;
	}
	public function get_ubi_dir_pais()
	{
		return $this->ubi_dir_pais;
	}
	public function get_ubi_dir_cont()
	{
		return $this->ubi_dir_cont;
	}
	public function get_ubi_nac_dist_desc()
	{
		return $this->ubi_nac_dist_desc;
	}
	public function get_ubi_nac_prov_desc()
	{
		return $this->ubi_nac_prov_desc;
	}
	public function get_ubi_nac_depa_desc()
	{
		return $this->ubi_nac_depa_desc;
	}
	public function get_ubi_nac_pais_desc()
	{
		return $this->ubi_nac_pais_desc;
	}
	public function get_ubi_nac_cont_desc()
	{
		return $this->ubi_nac_cont_desc;
	}
	public function get_ubi_nac_dist()
	{
		return $this->ubi_nac_dist;
	}
	public function get_ubi_nac_prov()
	{
		return $this->ubi_nac_prov;
	}
	public function get_ubi_nac_depa()
	{
		return $this->ubi_nac_depa;
	}
	public function get_ubi_nac_pais()
	{
		return $this->ubi_nac_pais;
	}
	public function get_ubi_nac_cont()
	{
		return $this->ubi_nac_cont;
	}

	public function get_fecha_fallecimiento()
	{
		return $this->fecha_fallecimiento;
	}
	public function get_ubi_fallecimiento_dist_desc()
	{
		$this->ubi_fallecimiento_dist_desc;
	}
	public function get_ubi_fallecimiento_prov_desc()
	{
		$this->ubi_fallecimiento_prov_desc;
	}
	public function get_ubi_fallecimiento_depa_desc()
	{
		$this->ubi_fallecimiento_depa_desc;
	}
	public function get_ubi_fallecimiento_pais_desc()
	{
		$this->ubi_fallecimiento_pais_desc;
	}
	public function get_ubi_fallecimiento_cont_desc()
	{
		$this->ubi_fallecimiento_cont_desc;
	}

	public function get_email()
	{
		return $this->email;
	}
	public function get_telefono()
	{
		return $this->telefono;
	}
	public function get_codigo_postal()
	{
		return $this->codigo_postal;
	}

	public function get_padre_nombre()
	{
		return $this->padre_nombre;
	}
	public function get_padre_apellido_paterno()
	{
		return $this->padre_apellido_paterno;
	}
	public function get_padre_apellido_materno()
	{
		return $this->padre_apellido_materno;
	}
	public function get_padre_apellido_matrimonio()
	{
		return $this->padre_apellido_matrimonio;
	}
	public function get_padre_nombre_completo()
	{
		return $this->padre_nombre_completo;
	}
	public function get_padre_tipo_doc()
	{
		return $this->padre_tipo_doc;
	}
	public function get_padre_num_doc()
	{
		return $this->padre_num_doc;
	}

	public function get_madre_nombre()
	{
		return $this->madre_nombre;
	}
	public function get_madre_apellido_paterno()
	{
		return $this->madre_apellido_paterno;
	}
	public function get_madre_apellido_materno()
	{
		return $this->madre_apellido_materno;
	}
	public function get_madre_apellido_matrimonio()
	{
		return $this->madre_apellido_matrimonio;
	}
	public function get_madre_nombre_completo()
	{
		return $this->madre_nombre_completo;
	}
	public function get_madre_tipo_doc()
	{
		return $this->madre_tipo_doc;
	}
	public function get_madre_num_doc()
	{
		return $this->madre_num_doc;
	}

	public function get_declarante_nombre()
	{
		return $this->declarante_nombre;
	}
	public function get_declarante_apellido_paterno()
	{
		return $this->declarante_apellido_paterno;
	}
	public function get_declarante_apellido_materno()
	{
		return $this->declarante_apellido_materno;
	}
	public function get_declarante_apellido_matrimonio()
	{
		return $this->declarante_apellido_matrimonio;
	}
	public function get_declarante_nombre_completo()
	{
		return $this->declarante_nombre_completo;
	}
	public function get_declarante_tipo_doc()
	{
		return $this->declarante_tipo_doc;
	}
	public function get_declarante_num_doc()
	{
		return $this->declarante_num_doc;
	}
	public function get_declarante_vinculo()
	{
		return $this->declarante_vinculo;
	}

	public function get_fecha_registro()
	{
		return $this->fecha_registro;
	}
	public function get_fecha_actualizacion()
	{
		return $this->fecha_actualizacion;
	}

	public function get_fuente_padron()
	{
		return $this->fuente_padron;
	}
	public function get_fuente_rrcc()
	{
		return $this->fuente_rrcc;
	}
	public function get_fuente_api()
	{
		return $this->fuente_api;
	}
	public function get_fuente_contacto()
	{
		return $this->fuente_contacto;
	}
}
