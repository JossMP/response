<?php
require_once((dirname(__DIR__)) . "/vendor/autoload.php");

$basic = new \jossmp\response\obj([
	'success' => false,
	'message' => 'Hola mundo cruel',
	'result' => ['a', 'b', 'c'],
]);

var_dump(isset($basic->message)); // true
var_dump(isset($basic->abc)); // false
var_dump(empty($basic->result)); // false
var_dump(!empty($basic->result)); // true
var_dump(count($basic->result)); // 3

$result = new \jossmp\response\model\person();
$result->set_control([
	'premiun' => true
]);
$result->load_data($basic);

var_dump($result);
exit();

$test0 = new \jossmp\response\obj(array(
	'fecha_actualizacion' => date("Y-m-d"),
	'fecha_nacimiento' => '1987-05-22',
	'link' => 'Este es un link',
));

$test1 = new \jossmp\response\obj([
	'fecha_actualizacion' => date("Y-m-d"),
	'padre_nombre' => 'RUPERTO',
	'madre_nombre' => 'BASILIA',
	'link2' => 'Este es un link',
]);

$test2 = new \jossmp\response\obj([
	'fecha_actualizacion' => date("Y-m-d"),
	'padre_nombre' => 'RUPERTO',
	'madre_nombre' => 'BASILIA LIDIA',
	'control' => [
		'api' => true
	],
]);

$result = new \jossmp\response\model\person();

$result->set_control([
	'premiun' => true
]);

$result->set_num_doc("44274795");
//$result->set_num_doc("44274790");
$result->load_data($test0);
$result->load_data($test1);
$result->load_data($test2);

echo $result->json(NULL, true);
$result->save_json(__DIR__ . '/json/example2.json');
